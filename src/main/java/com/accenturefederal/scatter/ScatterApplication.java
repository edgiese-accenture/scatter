package com.accenturefederal.scatter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com.accenturefederal")
public class ScatterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScatterApplication.class, args);
	}

}

