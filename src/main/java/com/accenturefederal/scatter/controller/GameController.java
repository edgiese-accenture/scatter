package com.accenturefederal.scatter.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.accenturefederal.scatter.model.Game;
import com.accenturefederal.scatter.model.Player;
import com.accenturefederal.scatter.service.GameService;
import com.accenturefederal.scatter.service.PlayerService;

class PieceInfo {
	Integer x;
	Integer y;
	public PieceInfo() {}
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}
	
}
class GameInfo {
	Long creator_id;
	String name;
	Integer field_x;
	Integer field_y;
	// List<PieceInfo> pieces;
	
	public GameInfo() {}

	public Long getCreator_id() {
		return creator_id;
	}

	public void setCreator_id(Long creator_id) {
		this.creator_id = creator_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getField_x() {
		return field_x;
	}

	public void setField_x(Integer field_x) {
		this.field_x = field_x;
	}

	public Integer getField_y() {
		return field_y;
	}

	public void setField_y(Integer field_y) {
		this.field_y = field_y;
	}

	/*
	public List<PieceInfo> getPieces() {
		return pieces;
	}

	public void setPieces(List<PieceInfo> pieces) {
		this.pieces = pieces;
	}
	*/
}

@RestController
public class GameController {

	@Autowired
	PlayerService playerService;
	@Autowired
	GameService gameService;
	
	@PostMapping(path = "/games", consumes = "application/json", produces = "application/json")
	public Object addGame(@RequestBody GameInfo gameInfo, Model model) {
	    Game game = new Game();
	    game.setName(gameInfo.name);
	    game.setField_x(gameInfo.field_x);
	    game.setField_y(gameInfo.field_y);
	    Optional<Player> creatorOption = playerService.getPlayer(gameInfo.creator_id);
	    if (!creatorOption.isPresent()) {
	    	// error -- creator is wrong
	    	
	    }
	    game.setCreator(creatorOption.get());
	    gameService.createGame(game);
	    return game;
	}
}
