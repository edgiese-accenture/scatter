package com.accenturefederal.scatter.controller;

import java.util.ArrayList;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.accenturefederal.scatter.model.Player;
import com.accenturefederal.scatter.restmodel.ErrorData;
import com.accenturefederal.scatter.restmodel.PlayerList;
import com.accenturefederal.scatter.restmodel.PlayerUpload;
import com.accenturefederal.scatter.service.PlayerService;

@RestController
public class PlayerController extends RestBase {
	@Autowired
	PlayerService playerService;

	@PostMapping(path = URIBASE + "/players", consumes = "application/json", produces = "application/json")
	public Object addPlayer(@RequestBody PlayerUpload playerInfo, Model model, HttpServletRequest request, HttpServletResponse response) {
	    Player player = new Player();
	    if (playerInfo.getName() == null) {
	    	response.setStatus(400);
	    	return ErrorData.rest400Error("Name is required");
	    }
	    if (playerInfo.getInitials() == null) {
	    	response.setStatus(400);
	    	return ErrorData.rest400Error("Initials are required");
	    }
	    player.setName(playerInfo.getName());
	    player.setInitials(playerInfo.getInitials());
	    playerService.createPlayer(player);
	    response.setStatus(201);
	    return playerService.playerData(player);
	}
	
	@PutMapping(path = URIBASE + "/players/{id}", consumes = "application/json", produces = "application/json")
	public Object editPlayer(
			@RequestBody PlayerUpload playerInfo, 
			@PathVariable Long id, 
			Model model, 
			HttpServletRequest request, 
			HttpServletResponse response
			) {
	    if (playerInfo.getName() == null) {
	    	response.setStatus(400);
	    	return ErrorData.rest400Error("Name is required");
	    }
	    if (playerInfo.getInitials() == null) {
	    	response.setStatus(400);
	    	return ErrorData.rest400Error("Initials are required");
	    }
	    Optional<Player> po = playerService.getPlayer(id);
	    
	    if (!po.isPresent()) {
	    	response.setStatus(404);
	    	return ErrorData.restErrorNoDetails(404, "Player id " + id + " not found.");
	    }
	    Player player = po.get();
	    
	    player.setName(playerInfo.getName());
	    player.setInitials(playerInfo.getInitials());
	    playerService.editPlayer(player);
	    return playerService.playerData(player);
	}
	
	@GetMapping(path = URIBASE + "/players", consumes = "application/json", produces = "application/json")
	public Object getPlayers(@RequestParam String name, Model model) {
		List<Player> players = playerService.
		PlayerList retval = new PlayerList();
		retval.setNum_players(0);
		retval.setPlayers(new ArrayList<>());
		return retval;
    }
}
