package com.accenturefederal.scatter.restmodel;

public class PlayerUpload {
	private String name;
	private String initials;
	
	public PlayerUpload() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}
}
