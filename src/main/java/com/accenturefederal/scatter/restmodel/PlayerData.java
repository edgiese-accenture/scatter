package com.accenturefederal.scatter.restmodel;

import java.util.List;

class PlayerHighScores {
	private Long game_id;
	private String game_name;
	private Integer high_score;
	
	public PlayerHighScores() {}

	public Long getGame_id() {
		return game_id;
	}

	public void setGame_id(Long game_id) {
		this.game_id = game_id;
	}

	public String getGame_name() {
		return game_name;
	}

	public void setGame_name(String game_name) {
		this.game_name = game_name;
	}

	public Integer getHigh_score() {
		return high_score;
	}

	public void setHigh_score(Integer high_score) {
		this.high_score = high_score;
	}
}

public class PlayerData {
	private Long player_id;
	private String name;
	private String initials;
	private List<GameData> created_games;
	private List<PlayerHighScores> high_scores;
	
	public PlayerData() {}

	public Long getPlayer_id() {
		return player_id;
	}

	public void setPlayer_id(Long player_id) {
		this.player_id = player_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public List<GameData> getCreated_games() {
		return created_games;
	}

	public void setCreated_games(List<GameData> created_games) {
		this.created_games = created_games;
	}

	public List<PlayerHighScores> getHigh_scores() {
		return high_scores;
	}

	public void setHigh_scores(List<PlayerHighScores> high_scores) {
		this.high_scores = high_scores;
	}
}
