package com.accenturefederal.scatter.restmodel;

import java.util.ArrayList;
import java.util.List;

public class ErrorData {
	int status;
	String message;
	List<Object> details;
	
	private ErrorData(int Status, String message, List<Object> details) {
		this.status = Status;
		this.message = message;
		this.details = details;
	}
	
	static public ErrorData rest400Error(String message) {
		return new ErrorData(400, message, new ArrayList<>());
	}
	static public ErrorData restErrorNoDetails(int status, String message) {
		return new ErrorData(status, message, new ArrayList<>());
	}
	
	public ErrorData() {}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Object> getDetails() {
		return details;
	}

	public void setDetails(List<Object> details) {
		this.details = details;
	}
}
