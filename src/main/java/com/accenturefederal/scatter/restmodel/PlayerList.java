package com.accenturefederal.scatter.restmodel;

import java.util.List;

public class PlayerList {
	int num_players;
	List<PlayerData> players;
	
	public PlayerList () {}
	
	public int getNum_players() {
		return num_players;
	}
	public void setNum_players(int num_players) {
		this.num_players = num_players;
	}
	public List<PlayerData> getPlayers() {
		return players;
	}
	public void setPlayers(List<PlayerData> players) {
		this.players = players;
	}
}
