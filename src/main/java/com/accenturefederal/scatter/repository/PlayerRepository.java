package com.accenturefederal.scatter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accenturefederal.scatter.model.Player;

@Repository
public interface PlayerRepository extends CrudRepository<Player, Long> {
}
