package com.accenturefederal.scatter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accenturefederal.scatter.model.Game;

@Repository
public interface GameRepository extends CrudRepository<Game, Long> {
}
