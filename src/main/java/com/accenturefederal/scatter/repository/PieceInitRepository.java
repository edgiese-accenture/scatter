package com.accenturefederal.scatter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accenturefederal.scatter.model.PieceInit;

@Repository
public interface PieceInitRepository extends CrudRepository<PieceInit, Long> {
}
