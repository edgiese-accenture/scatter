package com.accenturefederal.scatter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accenturefederal.scatter.model.GamePlay;

@Repository
public interface GamePlayRepository extends CrudRepository<GamePlay, Long> {
}
