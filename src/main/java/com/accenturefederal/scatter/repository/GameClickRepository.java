package com.accenturefederal.scatter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.accenturefederal.scatter.model.GameClick;

@Repository
public interface GameClickRepository extends CrudRepository<GameClick, Long> {
}
