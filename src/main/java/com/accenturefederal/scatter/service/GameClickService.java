package com.accenturefederal.scatter.service;

import java.util.Optional;

import com.accenturefederal.scatter.model.GameClick;

public interface GameClickService {
	GameClick createGameClick(GameClick GameClick);
	Optional<GameClick> getGameClick(Long id);
	GameClick editGameClick(GameClick GameClick);
	void deleteGameClick(GameClick GameClick);
	void deleteGameClick(Long id);
	Iterable<GameClick> getAllGameClicks();
	long countGameClicks();
}
