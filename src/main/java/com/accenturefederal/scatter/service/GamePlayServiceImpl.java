package com.accenturefederal.scatter.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenturefederal.scatter.model.GamePlay;
import com.accenturefederal.scatter.repository.GamePlayRepository;

@Service
public class GamePlayServiceImpl implements GamePlayService {
	private final GamePlayRepository gamePlayRepository;
	
	@Autowired
	public GamePlayServiceImpl(GamePlayRepository gamePlayRepository) {
		this.gamePlayRepository = gamePlayRepository;
	}


	@Override
	public GamePlay createGamePlay(GamePlay gamePlay) {
		return gamePlayRepository.save(gamePlay);
	}

	@Override
	public Optional<GamePlay> getGamePlay(Long id) {
		return gamePlayRepository.findById(id);
	}

	@Override
	public GamePlay editGamePlay(GamePlay gamePlay) {
		return gamePlayRepository.save(gamePlay);
	}

	@Override
	public void deleteGamePlay(GamePlay gamePlay) {
		gamePlayRepository.delete(gamePlay);
	}

	@Override
	public void deleteGamePlay(Long id) {
		gamePlayRepository.deleteById(id);
	}

	@Override
	public Iterable<GamePlay> getAllGamePlays() {
		return gamePlayRepository.findAll();
	}

	@Override
	public long countGamePlays() {
		return gamePlayRepository.count();
	}

}
