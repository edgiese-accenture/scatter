package com.accenturefederal.scatter.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenturefederal.scatter.model.PieceInit;
import com.accenturefederal.scatter.repository.PieceInitRepository;

@Service
public class PieceInitServiceImpl implements PieceInitService {
	private final PieceInitRepository pieceInitRepository;
	
	@Autowired
	public PieceInitServiceImpl(PieceInitRepository PieceInitRepository) {
		this.pieceInitRepository = PieceInitRepository;
	}


	@Override
	public PieceInit createPieceInit(PieceInit pieceInit) {
		return pieceInitRepository.save(pieceInit);
	}

	@Override
	public Optional<PieceInit> getPieceInit(Long id) {
		return pieceInitRepository.findById(id);
	}

	@Override
	public PieceInit editPieceInit(PieceInit pieceInit) {
		return pieceInitRepository.save(pieceInit);
	}

	@Override
	public void deletePieceInit(PieceInit pieceInit) {
		pieceInitRepository.delete(pieceInit);
	}

	@Override
	public void deletePieceInit(Long id) {
		pieceInitRepository.deleteById(id);
	}

	@Override
	public Iterable<PieceInit> getAllPieceInits() {
		return pieceInitRepository.findAll();
	}

	@Override
	public long countPieceInits() {
		return pieceInitRepository.count();
	}

}
