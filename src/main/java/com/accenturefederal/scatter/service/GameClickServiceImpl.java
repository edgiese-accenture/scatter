package com.accenturefederal.scatter.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenturefederal.scatter.model.GameClick;
import com.accenturefederal.scatter.repository.GameClickRepository;

@Service
public class GameClickServiceImpl implements GameClickService {
	private final GameClickRepository gameClickRepository;
	
	@Autowired
	public GameClickServiceImpl(GameClickRepository gameClickRepository) {
		this.gameClickRepository = gameClickRepository;
	}


	@Override
	public GameClick createGameClick(GameClick gameClick) {
		return gameClickRepository.save(gameClick);
	}

	@Override
	public Optional<GameClick> getGameClick(Long id) {
		return gameClickRepository.findById(id);
	}

	@Override
	public GameClick editGameClick(GameClick gameClick) {
		return gameClickRepository.save(gameClick);
	}

	@Override
	public void deleteGameClick(GameClick gameClick) {
		gameClickRepository.delete(gameClick);
	}

	@Override
	public void deleteGameClick(Long id) {
		gameClickRepository.deleteById(id);
	}

	@Override
	public Iterable<GameClick> getAllGameClicks() {
		return gameClickRepository.findAll();
	}

	@Override
	public long countGameClicks() {
		return gameClickRepository.count();
	}

}
