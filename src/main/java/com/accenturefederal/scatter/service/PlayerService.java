package com.accenturefederal.scatter.service;

import java.util.Optional;

import com.accenturefederal.scatter.model.Player;
import com.accenturefederal.scatter.restmodel.PlayerData;

public interface PlayerService {
	Player createPlayer(Player player);
	Optional<Player> getPlayer(Long id);
	Player editPlayer(Player player);
	void deletePlayer(Player player);
	void deletePlayer(Long id);
	Iterable<Player> getAllPlayers();
	long countPlayers();
	PlayerData playerData(Player player);
}