package com.accenturefederal.scatter.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenturefederal.scatter.model.Player;
import com.accenturefederal.scatter.repository.PlayerRepository;
import com.accenturefederal.scatter.restmodel.PlayerData;

@Service
public class PlayerServiceImpl implements PlayerService {
	private final PlayerRepository playerRepository;
	
	@Autowired
	private EntityManager em;	
	@Autowired
	private GameService gameService;
	
	@Autowired
	public PlayerServiceImpl(PlayerRepository playerRepository) {
		this.playerRepository = playerRepository;
	}

	@Override
	public Player createPlayer(Player player) {
		return playerRepository.save(player);
	}

	@Override
	public Optional<Player> getPlayer(Long id) {
		return playerRepository.findById(id);
	}

	@Override
	public Player editPlayer(Player player) {
		return playerRepository.save(player);
	}

	@Override
	public void deletePlayer(Player player) {
		playerRepository.delete(player);
	}

	@Override
	public void deletePlayer(Long id) {
		playerRepository.deleteById(id);
	}

	@Override
	public Iterable<Player> getAllPlayers() {
		return playerRepository.findAll();
	}

	@Override
	public long countPlayers() {
		return playerRepository.count();
	}
	
	public List<Player> findPlayerByName(final String name) {
		TypedQuery<Player> query =
				em.createQuery("SELECT p FROM Player p where p.name = :1", Player.class);
		query.setParameter(1, name);
		return query.getResultList();
	}
	
	@Override
	public PlayerData playerData(Player player) {
		PlayerData pd = new PlayerData();
		pd.setName(player.getName());
		pd.setInitials(player.getInitials());
		pd.setPlayer_id(player.getId());
		
		// TODO: do queries
		pd.setCreated_games(new ArrayList<>());
		pd.setHigh_scores(new ArrayList<>());
		
		return pd;
	}
}
