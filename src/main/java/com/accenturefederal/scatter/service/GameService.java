package com.accenturefederal.scatter.service;

import java.util.Optional;

import com.accenturefederal.scatter.model.Game;

public interface GameService {
	Game createGame(Game Game);
	Optional<Game> getGame(Long id);
	Game editGame(Game Game);
	void deleteGame(Game Game);
	void deleteGame(Long id);
	Iterable<Game> getAllGames();
	long countGames();
}
