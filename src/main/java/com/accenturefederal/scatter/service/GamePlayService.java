package com.accenturefederal.scatter.service;

import java.util.Optional;

import com.accenturefederal.scatter.model.GamePlay;

public interface GamePlayService {
	GamePlay createGamePlay(GamePlay GamePlay);
	Optional<GamePlay> getGamePlay(Long id);
	GamePlay editGamePlay(GamePlay GamePlay);
	void deleteGamePlay(GamePlay GamePlay);
	void deleteGamePlay(Long id);
	Iterable<GamePlay> getAllGamePlays();
	long countGamePlays();
}
