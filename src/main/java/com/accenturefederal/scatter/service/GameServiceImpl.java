package com.accenturefederal.scatter.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenturefederal.scatter.model.Game;
import com.accenturefederal.scatter.repository.GameRepository;

@Service
public class GameServiceImpl implements GameService {
	private final GameRepository gameRepository;
	
	@Autowired
	public GameServiceImpl(GameRepository GameRepository) {
		this.gameRepository = GameRepository;
	}


	@Override
	public Game createGame(Game game) {
		return gameRepository.save(game);
	}

	@Override
	public Optional<Game> getGame(Long id) {
		return gameRepository.findById(id);
	}

	@Override
	public Game editGame(Game game) {
		return gameRepository.save(game);
	}

	@Override
	public void deleteGame(Game game) {
		gameRepository.delete(game);
	}

	@Override
	public void deleteGame(Long id) {
		gameRepository.deleteById(id);
	}

	@Override
	public Iterable<Game> getAllGames() {
		return gameRepository.findAll();
	}

	@Override
	public long countGames() {
		return gameRepository.count();
	}

}
