package com.accenturefederal.scatter.service;

import java.util.Optional;

import com.accenturefederal.scatter.model.PieceInit;

public interface PieceInitService {
	PieceInit createPieceInit(PieceInit PieceInit);
	Optional<PieceInit> getPieceInit(Long id);
	PieceInit editPieceInit(PieceInit PieceInit);
	void deletePieceInit(PieceInit PieceInit);
	void deletePieceInit(Long id);
	Iterable<PieceInit> getAllPieceInits();
	long countPieceInits();
}
