package com.accenturefederal.scatter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name="game_click")
@Table(name="game_clicks")
public class GameClick {
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "x", nullable=false)
	private Integer x;
	
	@Column(name = "y", nullable=false)
	private Integer y;
	
	@Column(name = "time", nullable=false)
	private Long time;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="game_play_id", nullable=false)
	private GamePlay gamePlay;
	
	public GameClick() {}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}
	
	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public GamePlay getGamePlay() {
		return gamePlay;
	}

	public void setGamePlay(GamePlay gamePlay) {
		this.gamePlay = gamePlay;
	}
	
	@Override
	public String toString() {
		return "Game Click: x=" + x + " y=" + y + " time=" + time +" game_play_id=" + gamePlay.getId();
	}
}
