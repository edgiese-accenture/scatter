package com.accenturefederal.scatter.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;


@Entity(name="player")
@Table(name="players")
public class Player {
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "name", nullable=false)
	@Size(max = 50)
	private String name;
	
	@Column(name = "initials", nullable=false)
	@Size(max = 3)
	private String initials;

    @OneToMany(
    		fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name="creator_id")
    private Set<Game> games = new HashSet<>();
    
    public Player() {}
   
    public Set<Game> getGames() {
    	return games;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public Long getId() {
		return id;
	}
	
	public void addGame(Game game) {
		games.add(game);
		game.setCreator(this);
	}
	
	public void removeGame(Game game) {
		games.remove(game);
		game.setCreator(null);
	}
	
	@Override
	public String toString() {
		return String.format("Player{id=%d, name='%s', initials='%s', games=%s}", id, name, initials, games);
	}

}
