package com.accenturefederal.scatter.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;


@Entity(name="game")
@Table(name="games")
public class Game {
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "name", nullable=false)
	@Size(max = 50)
	private String name;
	
	@Column(name = "field_x", nullable=false)
	private Integer fieldX;
	
	@Column(name = "field_y", nullable=false)
	private Integer fieldY;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="creator_id", nullable=false)
	private Player creator;
	
    @OneToMany(
    		fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name="game_id")
    private Set<PieceInit> pieceInits = new HashSet<>();
    
	
	public Game() {}
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getField_x() {
		return fieldX;
	}

	public void setField_x(Integer field_x) {
		this.fieldX = field_x;
	}

	public Integer getField_y() {
		return fieldY;
	}

	public void setField_y(Integer field_y) {
		this.fieldY = field_y;
	}

	public Player getCreator() {
		return creator;
	}

	public void setCreator(Player creator) {
		this.creator = creator;
	}
	
	public Set<PieceInit> getPieceInits() {
		return pieceInits;
	}

	public void setPieceInits(Set<PieceInit> pieceInits) {
		this.pieceInits = pieceInits;
	}

	@Override
	public String toString() {
		return "Game: name="+name+" width="+fieldX+" height="+fieldY+" creator="+creator.getId();
	}
}
