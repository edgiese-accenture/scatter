package com.accenturefederal.scatter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name="piece_init")
@Table(name="piece_inits")
public class PieceInit {
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "x", nullable=false)
	private Integer x;
	
	@Column(name = "y", nullable=false)
	private Integer y;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="game_id", nullable=false)
	private Game game;
	
	public PieceInit() {}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	@Override
	public String toString() {
		return "Piece Init: x=" + x + " y=" + y + " game_id=" + game.getId();
	}
}
