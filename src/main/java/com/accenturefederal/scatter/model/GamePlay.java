package com.accenturefederal.scatter.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;


@Entity(name="game_play")
@Table(name="game_plays")
public class GamePlay {
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "score", nullable=false)
	private Integer score;
	
	@Column(name = "time", nullable=false)
	private Long time;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="game_id", nullable=false)
	private Game game;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="player_id", nullable=false)
	private Player player;
	
    @OneToMany(
    		fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name="game_play_id")
    private Set<GameClick> gameClicks = new HashSet<>();
    
	
	public GamePlay() {}
	
	public Long getId() {
		return id;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Long getTime() {
		return time;
	}
	
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	public Set<GameClick> getGameClicks() {
		return gameClicks;
	}

	public void setGameClicks(Set<GameClick> gameClicks) {
		this.gameClicks = gameClicks;
	}

	@Override
	public String toString() {
		return "GamePlay: id=" + id 
				+ " time="+time+" score="+score+" game="+
				game.getId()+" player=" + player.getId() + " clicks="+gameClicks;
	}
}
