package com.accenturefederal.scatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.GenericWebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.accenturefederal.scatter.controller.PlayerController;
import com.accenturefederal.scatter.service.PlayerService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ScatterApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ScatterRestTests {
	@LocalServerPort
	private int port;
	
	@Autowired
	private PlayerService playerService;
	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}	
	TestRestTemplate restTemplate = new TestRestTemplate();
	
	static protected final String BASEURI = "/api/v1.0";
	
	private ResponseEntity<String> addPlayer(int nPlayer) {
		String[] playerJsons = {
				// 0
				"{'name':'Allan Allbright', 'initials': 'aaa'}".replace('\'', '"'),
				// 1
				"{'name':'Boyce Brighton', 'initials': 'bbb'}".replace('\'', '"'),
				// 2
				"{'name':'Clara Cheung', 'initials': 'ccc'}".replace('\'', '"'),
				"{'name':'DeAnne Dittmar', 'initials': 'ddd'}".replace('\'', '"'),
				"{'name':'Evelyn Edgars', 'initials': 'eee'}".replace('\'', '"'),
				"{'name':'Frank Forzia', 'initials': 'fff'}".replace('\'', '"'),
				// 6. Do not add this name before the name list test.  Also leave in position (6)
				"{'name':'Gwen Gillworth', 'initials': 'ggg'}".replace('\'', '"')
		};
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<String> entity = new HttpEntity<>(playerJsons[nPlayer], headers);

		return (restTemplate.exchange(
				createURLWithPort(BASEURI + "/players"),
				HttpMethod.POST, entity, String.class));
	}

	@Test
	public void addTestPlayer0() throws Exception {
		ResponseEntity<String> response = this.addPlayer(0);
		ObjectNode node = new ObjectMapper().readValue(response.getBody(), ObjectNode.class);
		// status
		assertEquals("Status should be 201", HttpStatus.CREATED, response.getStatusCode());
		
		// result structure
		String[] topLevelShouldHaves = {"player_id", "name", "initials", "created_games", "high_scores"};
		for (final String shouldHave : topLevelShouldHaves) {
			assertTrue("Top level of Response should contain '" + shouldHave + "'", node.has(shouldHave));
		}
		assertEquals(node.get("name").asText(), "Allan Allbright");
		assertEquals(node.get("initials").asText(), "aaa");
	}
	
	@Test
	public void addPlayerMissingName() throws Exception {
		String json = "{'namee':'Zelda Zemecki', 'initials': 'zzz'}".replace('\'', '"');
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
				
		HttpEntity<String> entity = new HttpEntity<>(json, headers);

		final ResponseEntity<String> response1 =restTemplate.exchange(
				createURLWithPort(BASEURI + "/players"),
				HttpMethod.POST, entity, String.class);
		assertEquals("Status should be 400", HttpStatus.BAD_REQUEST, response1.getStatusCode());
	}
	@Test
	public void addPlayerMissingInitials() throws Exception {
		String json = "{'name':'Zelda Zemecki', 'initils': 'zzz'}".replace('\'', '"');
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
				
		HttpEntity<String> entity = new HttpEntity<>(json, headers);

		final ResponseEntity<String> response1 =restTemplate.exchange(
				createURLWithPort(BASEURI + "/players"),
				HttpMethod.POST, entity, String.class);
		assertEquals("Status should be 400", HttpStatus.BAD_REQUEST, response1.getStatusCode());
	}
	
	@Test
	public void putPlayer0() throws Exception {
		final ResponseEntity<String> response0 = this.addPlayer(1);
		assertEquals("Status should be 201", HttpStatus.CREATED, response0.getStatusCode());
		final ObjectNode node0 = new ObjectMapper().readValue(response0.getBody(), ObjectNode.class);
		assertTrue("Response should contain id", node0.has("player_id"));
		final Integer id = node0.get("player_id").asInt();
		
		String json = "{'name':'Zelda Zemecki', 'initials': 'zzz'}".replace('\'', '"');
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
				
		HttpEntity<String> entity = new HttpEntity<>(json, headers);

		final ResponseEntity<String> response1 =restTemplate.exchange(
				createURLWithPort(BASEURI + "/players/" + id.toString()),
				HttpMethod.PUT, entity, String.class);
		System.out.println(response1.getBody());
		assertEquals("Status should be 200", HttpStatus.OK, response1.getStatusCode());
		final ObjectNode node1 = new ObjectMapper().readValue(response1.getBody(), ObjectNode.class);
		assertTrue("Name got updated", node1.has("name") && node1.get("name").asText().equals("Zelda Zemecki"));
		assertTrue("Initials got updated", node1.has("initials") && node1.get("initials").asText().equals("zzz"));
	}
	@Test
	public void editPlayerMissingName() throws Exception {
		final ResponseEntity<String> response0 = this.addPlayer(1);
		assertEquals("Status should be 201", HttpStatus.CREATED, response0.getStatusCode());
		final ObjectNode node0 = new ObjectMapper().readValue(response0.getBody(), ObjectNode.class);
		assertTrue("Response should contain id", node0.has("player_id"));
		final Integer id = node0.get("player_id").asInt();

		String json = "{'namee':'Zelda Zemecki', 'initials': 'zzz'}".replace('\'', '"');
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
				
		HttpEntity<String> entity = new HttpEntity<>(json, headers);

		final ResponseEntity<String> response1 =restTemplate.exchange(
				createURLWithPort(BASEURI + "/players/" + id),
				HttpMethod.PUT, entity, String.class);
		assertEquals("Status should be 400", HttpStatus.BAD_REQUEST, response1.getStatusCode());
	}
	@Test
	public void editPlayerMissingInitials() throws Exception {
		final ResponseEntity<String> response0 = this.addPlayer(2);
		assertEquals("Status should be 201", HttpStatus.CREATED, response0.getStatusCode());
		final ObjectNode node0 = new ObjectMapper().readValue(response0.getBody(), ObjectNode.class);
		assertTrue("Response should contain id", node0.has("player_id"));
		final Integer id = node0.get("player_id").asInt();

		String json = "{'name':'Zelda Zemecki', 'initils': 'zzz'}".replace('\'', '"');
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
				
		HttpEntity<String> entity = new HttpEntity<>(json, headers);

		final ResponseEntity<String> response1 =restTemplate.exchange(
				createURLWithPort(BASEURI + "/players/" + id),
				HttpMethod.PUT, entity, String.class);
		assertEquals("Status should be 400", HttpStatus.BAD_REQUEST, response1.getStatusCode());
	}
	
	static String UTF_8_STRING = StandardCharsets.UTF_8.toString();
	
	private String myUrlEncode(String toEncode) {
		try {
			return URLEncoder.encode(toEncode, UTF_8_STRING);
		} catch (UnsupportedEncodingException e) {
			return "never happens";
		}
	}
	
	@Test
	public void getPlayerListEmpty() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
				
		HttpEntity<String> entity = new HttpEntity<>(headers);
		final ResponseEntity<String> response1 = restTemplate.exchange(
				createURLWithPort(BASEURI + "/players?name=" + myUrlEncode("Gwen Gillworth")),
				HttpMethod.GET,
				entity,
				String.class);
		assertEquals("Status should be 200", HttpStatus.OK, response1.getStatusCode());
		final ObjectNode node1 = new ObjectMapper().readValue(response1.getBody(), ObjectNode.class);
		assertTrue("Zero results returned", node1.has("num_players") && node1.get("num_players").asInt(0) == 0);
		assertTrue("empty Player results in table", node1.has("players") && node1.get("players").isArray());
	}
	
	@Test
	public void getPlayerListOneResult() throws Exception {
		this.addPlayer(6);  // should be Gwen Gillworth
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<String> entity = new HttpEntity<>(headers);
		final ResponseEntity<String> response1 = restTemplate.exchange(
				createURLWithPort(BASEURI + "/players?name=" + myUrlEncode("Gwen Gillworth")),
				HttpMethod.GET,
				entity,
				String.class);
		
		assertEquals("Status should be 200", HttpStatus.OK, response1.getStatusCode());
		final ObjectNode node1 = new ObjectMapper().readValue(response1.getBody(), ObjectNode.class);
		assertTrue("One result returned", node1.has("num_players") && node1.get("num_players").asInt(0) == 1);
		assertTrue("Player results in table", node1.has("players") && node1.get("players").isArray());
		
		final JsonNode playerList = node1.get("players");
		assertEquals("Should return one player", 1, playerList.size());
		
		final JsonNode player0 = playerList.get(0);
		// result structure
		String[] topLevelShouldHaves = {"player_id", "name", "initials", "created_games", "high_scores"};
		for (final String shouldHave : topLevelShouldHaves) {
			assertTrue("Player should contain '" + shouldHave + "'", player0.has(shouldHave));
		}
		assertEquals(player0.get("name").asText(), "Gwen Gillworth");
		assertEquals(player0.get("initials").asText(), "ggg");
	}
	
	@Test
	public void getPlayerListTwoResults() throws Exception {
		this.addPlayer(6);  // should be Gwen Gillworth
		/// ... should already have one "Gwen" from earlier test
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<String> entity = new HttpEntity<>(headers);
		final ResponseEntity<String> response1 = restTemplate.exchange(
				createURLWithPort(BASEURI + "/players?name=" + myUrlEncode("Gwen Gillworth")),
				HttpMethod.GET,
				entity,
				String.class);
		
		assertEquals("Status should be 200", HttpStatus.OK, response1.getStatusCode());
		final ObjectNode node1 = new ObjectMapper().readValue(response1.getBody(), ObjectNode.class);
		assertTrue("One result returned", node1.has("num_players") && node1.get("num_players").asInt(0) == 2);
		assertTrue("Player results in table", node1.has("players") && node1.get("players").isArray());
		
		final JsonNode playerList = node1.get("players");
		assertEquals("Should return one player", 2, playerList.size());
		
		final JsonNode player0 = playerList.get(1);
		// result structure
		String[] topLevelShouldHaves = {"player_id", "name", "initials", "created_games", "high_scores"};
		for (final String shouldHave : topLevelShouldHaves) {
			assertTrue("Player should contain '" + shouldHave + "'", player0.has(shouldHave));
		}
		assertEquals(player0.get("name").asText(), "Gwen Gillworth");
		assertEquals(player0.get("initials").asText(), "ggg");
	}

}
