package com.accenturefederal.scatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.accenturefederal.scatter.model.Game;
import com.accenturefederal.scatter.model.GameClick;
import com.accenturefederal.scatter.model.GamePlay;
import com.accenturefederal.scatter.model.PieceInit;
import com.accenturefederal.scatter.model.Player;
import com.accenturefederal.scatter.service.GameClickService;
import com.accenturefederal.scatter.service.GamePlayService;
import com.accenturefederal.scatter.service.GameService;
import com.accenturefederal.scatter.service.PieceInitService;
import com.accenturefederal.scatter.service.PlayerService;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ScatterDataServiceTests {
	
	@Autowired
	PlayerService playerService;
	
	@Autowired
	GameService gameService;
	
	@Autowired
	GamePlayService gamePlayService;
	
	@Autowired
	GameClickService gameClickService;
	
	@Autowired
	PieceInitService pieceInitService;
	
	@Autowired
	TestEntityManager entityManager;
	
	protected Player player1(boolean create) {
		Player p = new Player();
		p.setName("Ed Giese");
		p.setInitials("epg");
		if (create)
			playerService.createPlayer(p);
		return p;
	}
	
	@Test
	public void createPlayer() {
		Player p = player1(true);

		assertEquals("Ed Giese", p.getName());
		assertEquals("epg", p.getInitials());
		
		Player q = playerService.getPlayer(p.getId()).get();
		
		assertEquals("Ed Giese", q.getName());
		assertEquals("epg", q.getInitials());
		
	}
	
	protected Game game1(Player p, boolean pieces, boolean create) {
		Game g = new Game();
		g.setName("Easy Peasy");
		g.setField_x(1000);
		g.setField_y(500);
		g.setCreator(p);
		if (create)
			gameService.createGame(g);
		
		if (pieces) {
			// now add some piece inits
			PieceInit pi1 = new PieceInit();
			PieceInit pi2 = new PieceInit();
			pi1.setX(200);
			pi1.setY(250);
			pi2.setX(800);
			pi2.setY(250);
			pi1.setGame(g);
			pi2.setGame(g);
			if (create) {
				pieceInitService.createPieceInit(pi1);
				pieceInitService.createPieceInit(pi2);
			}
		}
		return g;
	}
	
	@Test
	public void createGame() {
		Player p = player1(true);
		Game g = game1(p, true, true);
		
		// force reload of objects from db:
		entityManager.flush();
		entityManager.clear();
		
		Game h = gameService.getGame(g.getId()).get();
		
		assertEquals("Ed Giese", h.getCreator().getName());
		assertEquals("Easy Peasy", h.getName());
		
		assertEquals("Piece init size", 2, h.getPieceInits().size());
		
		Player q = playerService.getPlayer(p.getId()).get();
		
		Set<Game> allGames = q.getGames();
		assertTrue("Ed Giese made one game", allGames.size() == 1);
		assertTrue("Easy Peasy should be made by Ed Giese", ((Game)(allGames.toArray()[0])).getName().equals("Easy Peasy"));
	}
	
	@Test
	public void createGamePlay() {
		Player p = player1(true);
		Game g = game1(p, true, true);
		
		GamePlay gp = new GamePlay();
		gp.setPlayer(p);
		gp.setGame(g);
		gp.setScore(500);
		gp.setTime(750L);
		gamePlayService.createGamePlay(gp);
		
		
		GameClick gc = new GameClick();
		gc.setTime(0L);
		gc.setX(191);
		gc.setY(250);
		gc.setGamePlay(gp);
		gameClickService.createGameClick(gc);
		
		// force reload of objects from db:
		entityManager.flush();
		entityManager.clear();
		
		GamePlay gp2 = gamePlayService.getGamePlay(gp.getId()).get();
		
		assertEquals("Easy Peasy", gp2.getGame().getName());
		assertEquals("Game Clicks should be 1", 1, gp2.getGameClicks().size());
	}

}
