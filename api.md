API Documentation
=================

The API at this point is really intended for just this game app.  The base of the API is

```
<domain>/api/v1.0/
```

Most of the entry points can return errors.  The error codes will always have a request body in the following format:

```
{
	"status": <returned error status>,
	"message": "Error Message",
	"details": { <error specific details.  could be empty.> }
}
```


## Entry points for ````games````

A game will have the following structure:

```
{
	"name": "Easy Peasy",
	"creator": {
		"id": 1234,
		"name": "John Q. Smith",
		"initials": "jqs"
	},
	"number_of_plays": 5280,
	"field_x": 1000,
	"field_y": 500,
	"pieces": [
		{"x": 200, "y": 250},
		{"x": 400, "y": 250}
	],
	"high_scores": [
		{"player_id": <id>, "player_initials": "abc", "score": 123},
		...
	]
}
```

### Create a new game

````
 POST /games
````

The body of the post should be the game in the following format:


```
{
	"name": "Easy Peasy",
	"field_x": 1000,
	"field_y": 500,
	"pieces": [
		{"x": 200, "y": 250},
		{"x": 400, "y": 250}
	]
}
```

#### Return Statuses

* ``` 201 ``` Created.  Body of response is JSON of newly created game.
* ``` 409 ``` Game name already in use.
* ``` 400 ``` Missing or erroneous input.


### Edit a game

```
 PUT /games/id
```

The body of the put should be the entire game, with any edits in place. The play counts, high scores, and creator data may be present in the body, but will be ignored, as these are read-only fields for editing purposes.

#### Return Statuses

* ``` 200 ``` Successful update.  Body of response is updated game.
* ``` 404 ``` ID does not correspond to a game.
* ``` 400 ``` Badly formed game.


### Get a list of games

```
 GET /games?popularity=[high|all]&creator=<id>
```

Both ```popularity``` and ```creator``` are optional.  If omitted, the api will return all results.  So, obviously, ```all``` is never necessary for popularity.  The body of the return will be a list of games in the following format:

````
[
	{ <game ... see format above> },
	{ <game ... see format above> },
	...
]
````

#### Return Statuses

* ``` 200 ``` Successful GET.  Body contains returned game list.
* ``` 404 ``` No games match criteria.
* ``` 400 ``` Creator missing or unknown popularity type.


### Get a single game

```
 GET /games/<id>
```

Return body is a single game as shown above.

#### Return Statuses

* ``` 200 ``` Successful GET.  Body contains returned game.
* ``` 404 ``` No game found with this id.



## Entry points for ```Players```

A player will have the following structure:

```
{
	"player_id": 0,
	"name": "John Q. Smith",
	"initials": "jqs",
	"created_games": [
		{ <game ... see format above> },
		{ <game ... see format above> },
		...
	],
	"high_scores": [
		{"game_id": <game_id>, "game_name": "game name", "high_score": <score>},
		...
	]
}
```

### Create a new player

````
 POST /players
````

The body of the post should be the name.

#### Return Statuses

* ``` 201 ``` Created.  Body of response is JSON of newly created player.
* ``` 400 ``` Missing or erroneous input.


### Edit a player

```
 PUT /players/id
```

The following body shows what is allowed to be edited in a player.  All other fields are calculated:

```
{
	"name": "John Q. Smith",
	"initials": "jqs",
}
```


#### Return Statuses

* ``` 200 ``` Successful update.  Body of response is updated player.
* ``` 404 ``` ID does not correspond to a player.
* ``` 400 ``` Badly formed player.


### Get a list of players

```
 GET /players?name=<url encoded name>
```

The ```name``` is optional.  If omitted, All players are returned.  The body of the return will be a list of players in the following format:

````
[
	{ <player ... see format above> },
	{ <player ... see format above> },
	...
]
````

#### Return Statuses

* ``` 200 ``` Successful GET.  Body contains returned player list.
* ``` 404 ``` No players match criteria.


### Get a single player

```
 GET /players/<id>
```

Return body is a single player as shown above.

#### Return Statuses

* ``` 200 ``` Successful GET.  Body contains returned player.
* ``` 404 ``` No player found with this id.


## Entry points for ```game_plays```

A game_play will have the following structure:

```
	"new_high_score": <true or false>,
	"score": <score>,
	"time": <time in milliseconds>,
	"player": {
		"name": "John Q. Smith",
		"initials": "jqs",
		"created_games": [
			{ <game ... see format above> },
			{ <game ... see format above> },
			...
		],
		"high_scores": [
			{"game_id": <game_id>, "game_name": "game name", "high_score": <score>},
			...
		]
	}, 
	"game": {
		"name": "Easy Peasy",
		"creator": {
			"id": 1234,
			"name": "John Q. Smith",
			"initials": "jqs"
		},
		"number_of_plays": 5280,
		"field_x": 1000,
		"field_y": 500,
		"pieces": [
			{"x": 200, "y": 250},
			{"x": 400, "y": 250}
		],
		"high_scores": [
			{"player_id": <id>, "player_initials": "abc", "score": 123},
			...
		]
	}
```

### Create a new game_play

````
 POST /game_plays
````

The body of the request should be in the following format:

```
{
	"player_id": <id>,
	"game_id": <id>,
	"score": <score>,
	"time": <time in milliseconds>
}
```

#### Return Statuses

* ``` 201 ``` Created.  Body of response is JSON of newly created game_play.
* ``` 400 ``` Missing or erroneous input.

